1. Zadania
https://www.hackerrank.com/domains/data-structures
https://www.hackerrank.com/domains/fp
https://www.codewars.com/kata/latest/my-languages

2. Dokumentacja

2.1 Iteratory / generatory
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol/iterator
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols

2.2 Map
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map

2.3 Set
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/set

2.4 Tablice
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduceRight
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/keys

2.5 Weak(Map|Set|Ref)
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakRef

Dodatkowe materiały:
https://javascript.info/array
https://javascript.info/iterable
https://javascript.info/generators
https://javascript.info/map-set
https://javascript.info/weakmap-weakset
https://javascript.info/keys-values-entries
https://javascript.info/destructuring-assignment