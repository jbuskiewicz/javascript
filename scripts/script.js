// dynamiczne definiowanie obiektu
// const person = {
//   name: "Jan",
//   lastName: "Kowalski",
//   introduce() {
//     console.log("...");
//   },
// };

// --------------------------------------------------------------------------------

// obiekt tworzony na bazie prototypu
// const person2 = Object.create(person);
// console.log(person2);

// kopiowanie wartosci z target do source
// const person3 = Object.assign({}, {});

// operator spread - kopiowanie
// const person4 = { ...person };

// funkcja konstruktora

// "use strict";
// syntax-sugar
class Phone {
  // konstruktor
  constructor(marka, model) {
    this.marka = marka;
    this.model = model;
  }

  printDetails() {
    console.log(`Marka telefonu: ${this.marka}, model: ${this.model}`);
  }
}

function Phone2(marka, model) {
  this.marka = marka;
  this.model = model;
}
function test() {
  bar = "lorem ipsum dolor";
  console.log(this); // undefined jesli mamy tryb strict
}
// test();
// new
const phone1 = new Phone("Nokia", "3310i");
const phone2 = new Phone2("Nokia", "nGage");

// console.log(phone1, phone2);
const o2 = {}; // o2 = new Object();

// przekazanie kontekstu - binding

// function logger(arg) {
//   console.log(arg);
// }

// const printDetails = phone1.printDetails;
// printDetails.bind(phone2)();

// bind => przekazuje kontekst, bez wywolania
// call, apply => przekazujemy argumenty, apply (array), call (po przecinku) => od razu wywolujemy metode

// --------------------------------------------------------------------------------

// Krok 1 - tworzenie obiektu w sposób dynamiczny
// let o2 = {};
// o2.name = "";
// o2.type = "";
// o2.display = function () {};

// --------------------------------------------------------------------------------

// Krok 2 - funkcje konstruktora

// function User(username, mail, avatarUrl) {
//   let user = {};
//   user.username = username;
//   user.mail = mail;
//   user.avatarUrl = avatarUrl;

//   user.login = function () {
//     console.log(`Logujemy usera ${this.username}...`);
//   };

//   return user;
// }

// const u1 = new User("user1", "user1@test.pl", "https://...");
// const u2 = new User("user2", "user2@test.pl", "https://...");
// const u3 = new User("user3", "user3@test.pl", "https://...");
// const u4 = new User("user4", "user4@test.pl", "https://...");
// const u5 = new User("user5", "user5@test.pl", "https://...");
// const u6 = new User("user6", "user6@test.pl", "https://...");
// const u7 = new User("user7", "user7@test.pl", "https://...");
// const u8 = new User("user8", "user8@test.pl", "https://...");

// console.log(u1, u2, u3, u4, u5);

// --------------------------------------------------------------------------------

// Krok 3
// Tworzymy obiekt, który będzie współdzielił wszystkie metody które chcemy dodać do naszej klasy

// const userMethods = {
//   login: function () {
//     console.log(`Logujemy usera ${this.username}...`);
//   },
//   logout: function () {
//     console.log(`Wylogowuje usera ${this.username}...`);
//   },
// };

// function User(username, mail, avatarUrl) {
//   let user = {};
//   user.username = username;
//   user.mail = mail;
//   user.avatarUrl = avatarUrl;

//   user.login = userMethods.login;
//   user.logout = userMethods.logout;

//   return user;
// }

// const u1 = new User("user1", "user1@test.pl", "https://...");
// const u2 = new User("user2", "user2@test.pl", "https://...");
// const u3 = new User("user3", "user3@test.pl", "https://...");
// const u4 = new User("user4", "user4@test.pl", "https://...");
// const u5 = new User("user5", "user5@test.pl", "https://...");
// const u6 = new User("user6", "user6@test.pl", "https://...");
// const u7 = new User("user7", "user7@test.pl", "https://...");
// const u8 = new User("user8", "user8@test.pl", "https://...");

// console.log(u1, u2, u3, u4, u5);

// --------------------------------------------------------------------------------

// Krok 4
// Szukamy sposobu na wspodzielenie metod bez koniecznosci ich ponownej implementacji w funkcji User

const userMethods = {
  login: function () {
    console.log(`Logujemy usera ${this.username}...`);
  },
  logout: () => {
    console.log(`Wylogowuje usera ${this.username}...`);
  },
};

function User(username, mail, avatarUrl) {
  let user = Object.create(userMethods); // userMethods staje sie prototypem dla obiektu user
  user.username = username;
  user.mail = mail;
  user.avatarUrl = avatarUrl;

  return user;
}

const u8 = new User("user8", "user8@test.pl", "https://...");
console.log(u8);
u8.login();

const car = (color) => {
  console.log(this);
  this.color = color;
};

// const porsche = new Car("red");
// console.log(porsche);
// car();
