1. Asynchroniczność

Wstęp do asynchronicznego JS
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Introducing

Promises - obietnice
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise

Kilka przykładów wykorzystania Promises
https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Implementing_a_promise-based_API

Funkcje asynchroniczne
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function

Await
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await

Async/await
https://javascript.info/async-await

2. Pętla zdarzeń - Event Loop

Event Loop
https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop

3. Komunikacja z BE

Fetch
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API

XMLHttpRequest
https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest

4. Symbole
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol
https://www.programiz.com/javascript/symbol
https://www.javascripttutorial.net/es6/symbol/

5. Asynchroniczna iteracja
https://javascript.info/async-iterators-generators

6. Ładowanie skryptów - async a defer
https://javascript.info/script-async-defer

7. Proxy
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy
https://blog.logrocket.com/practical-use-cases-for-javascript-es6-proxies/
https://www.javascripttutorial.net/es6/javascript-proxy/
https://javascript.info/proxy

Dodatkowe materiały:
https://eloquentjavascript.net/11_async.html
https://nodejs.dev/learn/javascript-asynchronous-programming-and-callbacks
https://www.youtube.com/watch?v=cCOL7MC4Pl0&ab_channel=JSConf - Event Loop, JSConf
https://blog.sessionstack.com/how-javascript-works-event-loop-and-the-rise-of-async-programming-5-ways-to-better-coding-with-2f077c4438b5
https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage (komunikacja między aplikacjami np. mikrofrontend)

Poza scope:
https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API
https://www.youtube.com/watch?v=IB5yxGGVEGk&ab_channel=PROIDEAEvents - Strumienie